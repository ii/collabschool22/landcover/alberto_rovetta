import numpy as np
import matplotlib.pyplot as plt

#files and the code should be in the same dir
PATH_TO_WORLD_MAP = "gl-latlong-1km-landcover.bsq"
PATH_TO_COORDINATES = "events_4.5.txt"

# data1 = np.fromfile(PATH, dtype='uint8')
world_map = np.memmap(PATH_TO_WORLD_MAP, dtype='uint8')
#coordinates = np.fromfile(PATH_TO_COORDINATES,)
# print(data1.shape)
# print(data2.shape)
reshaped = world_map.reshape(21600, 43200)

def is_this_land(SN,WE,reshaped=reshaped):
    '''Input x and y in geographical coordinates
    print id that point correspont to land or water
     based on loaded map'''

    SN_map = int(((SN + 90)/180)*21599)
    WE_map = int(((WE + 180)/360)*43199)

    if reshaped[SN_map,WE_map] != 0:
        print('Land')
    else:
        print('Water')


#plt.imshow(reshaped)
#plt.show()

SN = 50
WE = -24

is_this_land(SN,WE,reshaped)

def test_is_this_land():
    assert is_this_land(100,100)==True





